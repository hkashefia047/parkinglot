package com.company.hossein;

/**
 * Parking
 */
public class Parking  {

    private Agent agent;
    private Car car;
    private int TimeOfEntrance;
    private int TimeOfExit;

    public Parking() {
    }

    public Parking(Agent agent, Car car, int timeOfEntrance, int timeOfExit) {
        this.agent = agent;
        this.car = car;
        TimeOfEntrance = timeOfEntrance;
        TimeOfExit = timeOfExit;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public int getTimeOfEntrance() {
        return TimeOfEntrance;
    }

    public void setTimeOfEntrance(int timeOfEntrance) {
        TimeOfEntrance = timeOfEntrance;
    }

    public int getTimeOfExit() {
        return TimeOfExit;
    }

    public void setTimeOfExit(int timeOfExit) {
        TimeOfExit = timeOfExit;
    }

    @Override
    public String toString() {
        return "Parking TimeOfEntrance=" + TimeOfEntrance + ", TimeOfExit=" + TimeOfExit ;
                
    }

    

    
}