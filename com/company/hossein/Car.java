package com.company.hossein;

public class Car  {
    private String Color;
    private int pelak;
    public Car() {
    }

    public Car(String color, int pelak) {
        Color = color;
        this.pelak = pelak;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public int getPelak() {
        return pelak;
    }

    public void setPelak(int pelak) {
        this.pelak = pelak;
    }

    @Override
    public String toString() {
        return "Car [Color=" + Color + ", pelak=" + pelak + "]";
    }
    
    
}
